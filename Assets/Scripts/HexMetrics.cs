﻿using UnityEngine;

public static class HexMetrics
{
    public const float outerRadius = 10f;
    // (outerRadius * root of 3) / 2
    public const float innerRadius = outerRadius * 0.866025404f;

	public static int cornersCount = 6;
    public static Vector3[] corners = {
		new Vector3(0f, 0f, outerRadius),
		new Vector3(innerRadius, 0f, 0.5f * outerRadius),
		new Vector3(innerRadius, 0f, -0.5f * outerRadius),
		new Vector3(0f, 0f, -outerRadius),
		new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
		new Vector3(-innerRadius, 0f, 0.5f * outerRadius),
		new Vector3(0f, 0f, outerRadius),
	};
}
